Feature: Calculator calculates

  Scenario: Calculate plus
    Given expression is 1.0 "+" 2.0
    When calculate is pressed
    Then displays 3.0

  Scenario: Calculate minus
    Given expression is 5.1 "-" 1.1
    When calculate is pressed
    Then displays 4.0

  Scenario: Calculate divide
    Given expression is 52 "/" 10
    When calculate is pressed
    Then displays 5.2