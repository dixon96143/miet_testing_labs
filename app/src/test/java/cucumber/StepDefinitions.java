package cucumber;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.assertj.core.data.Offset;
import testing_miet.CalculatorPresenter;
import testing_miet.domain.CalculatorModel;
import testing_miet.ui.CalculatorView;

import static org.assertj.core.api.Assertions.assertThat;

public class StepDefinitions {

    private final CalculatorModel model = new CalculatorModel();

    private final CalculatorView view = new CalculatorView();

    private final CalculatorPresenter presenter = new CalculatorPresenter(model, view);

    @Given("expression is {double} {string} {double}")
    public void expression_is(Double double1, String string, Double double2) {
        presenter.start();
        view.textField().setText(double1.toString() + " " + string + " " + double2.toString());
        sleep(2);
    }

    @When("calculate is pressed")
    public void calculate_is_pressed() {
        view.button().doClick(500);
        sleep(1);
    }

    @Then("displays {double}")
    public void displays(Double double1) {
        assertThat(double1).isCloseTo(Double.valueOf(view.label().getText()), Offset.offset(0.001));
    }

    private static void sleep(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }


}