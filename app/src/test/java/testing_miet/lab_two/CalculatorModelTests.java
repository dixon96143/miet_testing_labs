package testing_miet.lab_two;

import org.assertj.core.data.Offset;
import org.junit.Test;
import testing_miet.domain.CalculatorExpression;
import testing_miet.domain.CalculatorModel;

import static org.assertj.core.api.Assertions.assertThat;
import static testing_miet.domain.CalculatorOperation.*;

public class CalculatorModelTests {

    private final CalculatorModel model = new CalculatorModel();

    @Test
    public void shouldCalculatePlus() {
        // given
        var leftOperand = 1.0;
        var rightOperand = 2.0;
        var operation = PLUS;
        var expression = new CalculatorExpression(leftOperand, rightOperand, operation);

        // when
        var result = this.model.calculate(expression);

        // then
        assertThat(result.result()).isPresent();
        assertThat(result.result().get()).isCloseTo(3.0, defaultOffset());
    }

    @Test
    public void shouldCalculateMinus() {
        // given
        var leftOperand = 1.0;
        var rightOperand = 2.0;
        var operation = MINUS;
        var expression = new CalculatorExpression(leftOperand, rightOperand, operation);

        // when
        var result = this.model.calculate(expression);

        // then
        assertThat(result.result()).isPresent();
        assertThat(result.result().get()).isCloseTo(-1.0, defaultOffset());
    }

    @Test
    public void shouldCalculateMultiply() {
        // given
        var leftOperand = 1.0;
        var rightOperand = 2.0;
        var operation = MULTIPLY;
        var expression = new CalculatorExpression(leftOperand, rightOperand, operation);

        // when
        var result = this.model.calculate(expression);

        // then
        assertThat(result.result()).isPresent();
        assertThat(result.result().get()).isCloseTo(2.0, defaultOffset());
    }

    @Test
    public void shouldCalculateDivide() {
        // given
        var leftOperand = 1.0;
        var rightOperand = 2.0;
        var operation = DIVIDE;
        var expression = new CalculatorExpression(leftOperand, rightOperand, operation);

        // when
        var result = this.model.calculate(expression);

        // then
        assertThat(result.result()).isPresent();
        assertThat(result.result().get()).isCloseTo(0.5, defaultOffset());
    }

    @Test
    public void shouldReturnEmptyResultWhenDivideByZero() {
        // given
        var leftOperand = 1.0;
        var rightOperand = 0;
        var operation = DIVIDE;
        var expression = new CalculatorExpression(leftOperand, rightOperand, operation);

        // when
        var result = this.model.calculate(expression);

        // then
        assertThat(result.result()).isEmpty();
        assertThat(result.errorMessage()).isPresent();
    }

    private static Offset defaultOffset() {
        return Offset.offset(0.00001);
    }
}
