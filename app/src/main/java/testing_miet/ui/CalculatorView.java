package testing_miet.ui;

import javax.swing.*;
import java.awt.*;

public class CalculatorView {

    private static final int WINDOW_WIDTH = 500;

    private static final int WINDOW_HEIGHT = 500;

    private static final Font FONT = new Font("Arial", Font.BOLD, 30);

    private final JFrame frame;

    private final JTextField textField;

    private final JButton button;

    private final JLabel label;

    public CalculatorView() {
        this.frame = new JFrame("Calculator");
        this.textField = new JTextField("");
        this.button = new JButton("Calculate expression");
        this.label = new JLabel("");
    }

    public void setup() {
        // Frame setup
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        frame.setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));

        // TextField setup
        textField.setColumns(20);
        textField.setFont(FONT);
        textField.setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT / 3));

        // Button setup
        button.setFont(FONT);
        button.setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT / 3));

        // Label setup
        label.setFont(FONT);
        label.setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT / 3));

        frame.add(textField);
        frame.add(button);
        frame.add(label);
    }

    public void display() {
        this.frame.setVisible(true);
    }

    public JTextField textField() {
        return textField;
    }

    public JButton button() {
        return button;
    }

    public JLabel label() {
        return label;
    }
}
