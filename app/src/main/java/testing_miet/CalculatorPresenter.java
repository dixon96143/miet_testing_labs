package testing_miet;

import testing_miet.domain.CalculatorModel;
import testing_miet.domain.ExpressionParser;
import testing_miet.ui.CalculatorView;

import java.awt.event.ActionEvent;

public class CalculatorPresenter {

    private final CalculatorModel model;

    private final CalculatorView view;

    public CalculatorPresenter(CalculatorModel model, CalculatorView view) {
        this.model = model;
        this.view = view;
    }

    public void start() {
        view.setup();
        view.display();
        view.button().addActionListener(this::buttonClicked);
    }

    public void buttonClicked(ActionEvent e) {
        var inputText = view.textField().getText();
        try {
            var expression = ExpressionParser.from(inputText);
            var result = this.model.calculate(expression);
            result.result().ifPresent(value -> view.label().setText(value.toString()));
            result.errorMessage().ifPresent(message -> view.label().setText(message));
        }
        catch (IllegalArgumentException exception) {
            view.label().setText(exception.getMessage());
        }
    }

}
