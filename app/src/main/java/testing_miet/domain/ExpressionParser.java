package testing_miet.domain;

import java.util.regex.Pattern;

import static testing_miet.domain.CalculatorOperation.*;

public class ExpressionParser {

    private static final Pattern PATTERN = Pattern.compile("(\\d+\\.?\\d*)\\s*([+,\\-,*,/])\\s*(\\d+\\.?\\d*)");

    public static CalculatorExpression from(String input) throws IllegalArgumentException {
        var matcher = PATTERN.matcher(input);
        if (matcher.matches()) {
            try {
                var leftOperand = Double.parseDouble(matcher.group(1));
                var rightOperand = Double.parseDouble(matcher.group(3));
                return switch (matcher.group(2)) {
                    case "+" -> new CalculatorExpression(leftOperand, rightOperand, PLUS);
                    case "-" -> new CalculatorExpression(leftOperand, rightOperand, MINUS);
                    case "/" -> new CalculatorExpression(leftOperand, rightOperand, DIVIDE);
                    case "*" -> new CalculatorExpression(leftOperand, rightOperand, MULTIPLY);
                    default -> throw parseException("Cannot parse operator");
                };
            } catch (NumberFormatException e) {
                throw parseException("Cannot parse numbers in expression");
            }
        } else {
            throw parseException("Cannot parse numbers");
        }
    }

    private static IllegalArgumentException parseException(String errorMessage) {
        throw new IllegalArgumentException(errorMessage);
    }
}
